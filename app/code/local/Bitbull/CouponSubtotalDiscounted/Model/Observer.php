<?php
/**
 * @category Bitbull
 * @package  Bitbull_CouponSubtotalDiscounted
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_CouponSubtotalDiscounted_Model_Observer
{
    /**
     * Chiamato all'evento "salesrule_rule_condition_combine".
     * Aggiunge la regola alla lista di quelle disponibili.
     *
     * @param Varien_Event_Observer $observer
     * @return Bitbull_CouponSubtotalDiscounted_Model_Observer
     */
    public function addRule(Varien_Event_Observer $observer)
    {
        $additional = $observer->getAdditional();

        $additionalConditions = array(
            array(
                'value' => 'Bitbull_CouponSubtotalDiscounted_Model_Rule_Condition_Subtotaldiscount',
                'label' => Mage::helper('salesrule')->__('Subtotal with discount')
            )
        );

        $additional->setConditions($additionalConditions);

        return $this;
    }
}