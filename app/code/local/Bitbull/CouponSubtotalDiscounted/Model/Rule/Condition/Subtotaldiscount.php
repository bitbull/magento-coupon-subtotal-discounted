<?php
/**
 * @category Bitbull
 * @package  Bitbull_CouponSubtotalDiscounted
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_CouponSubtotalDiscounted_Model_Rule_Condition_Subtotaldiscount
    extends Mage_Rule_Model_Condition_Abstract
{
    const RULE_CODE = 'base_subtotal_with_discount';
    
    public function loadAttributeOptions()
    {
        $attributes = array(
            self::RULE_CODE => Mage::helper('salesrule')->__('Subtotal with discount')
        );

        $this->setAttributeOption($attributes);

        return $this;
    }

    public function getAttributeElement()
    {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }

    public function getInputType()
    {
        return 'numeric';
    }

    public function getValueElementType()
    {
        return 'text';
    }


    /**
     * Validate Address Rule Condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        if (self::RULE_CODE == $this->getAttribute()) {
            $object->setBaseSubtotalWithDiscount($object->getBaseSubtotal() + $object->getDiscountAmount());
        }

        return parent::validate($object);
    }


}
